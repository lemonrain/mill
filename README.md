# Mill

## Project discreption
We are programming the board game mill, as an assignment.The goal is that two players can play on one computer together taking turns.\
The gist of the game is to lay stones on the board and to lay mills(get three stones in one row not diagonal) then you can take a stone from the other player. One player has won when the other has only two stones left.\
Therefore we are building a gui to have a board to play in. We are not using buttons as a way to lay stones on the board rather we let the players make points on the board with their mouse and the node which is nearest to the point they picked is chosen to put a stone on or move the stone.\
In the game are three phases which are to be played.\
In the first one the players lay the stones on the nodes of the board and try to build mills.\
In the second one all stones are on the board and the players can start to move the stones.\
In the third one player or both have only three stones left and can start to "jump" and bring one of the stones in any position they wish to.
For reference have a look at [this page](http://www.mathematische-basteleien.de/muehle.htm).

## How it works
To start the game the players open the mill class and compile the code. There should open a window telling the players who is starting the game player 0 or player 1. The players can also see in the upper left who's turn it is and which color of stone the person has.\
In the first phase the players have to set their stones on the board, therefore they just click on the node where they want the stone to be, taking turns. If one of the players makes a mill this person can select an already played stone, this one must not be in a mill, by clicking on it and the stone is out of the game. When both players have layed down all there stones. The second phase starts:\
Here the players want to move their already laid stones. With the mouse they click on the one they want to move and than click on the node they want to move it to. But they can only set the stone left, right and above or under in some cases, otherwise they have to choose their stone again and try another position. If someone lays again a bridge they can again pick one of the stones of their oponent. But only one even when they by any chance laid two mills at once.\
In the third and final phase, one of the players has only three stones left and is now allowed so put their stones anywhere they want too. Both can only have three left or only one. The one to loose is the first one who only has to stones left.\
The winner is anounced over a pop-up and then over another pop-up the players can decide if they want to play again.


## Built with 
Java

**Niklas Menge, Sabrina Viel**

