/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author Mill-Project-Developing-Team
 */
public class MillPanel extends JPanel implements MouseListener {
    
    private ArrayList<Paintable> painting = new ArrayList<Paintable>();
    private Color backgroundcolor = Color.WHITE;
    private GameInfo gameInfo;
    
    public MillPanel(Color backgroundcolor){
        
        setBorder(BorderFactory.createLineBorder(Color.black));
        
        this.backgroundcolor = backgroundcolor;
        
        addMouseListener(this);
        
    }
    
    public MillPanel(){
        
        setBorder(BorderFactory.createLineBorder(Color.black));
        
        addMouseListener(this);
        
    }
    
    public Dimension getPreferredSize(){
        
        return new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width,Toolkit.getDefaultToolkit().getScreenSize().height);
        
    }
    
    public void setPainting(ArrayList<Paintable> painting){
        
        this.painting = painting;
        
        this.paintComponent(this.getGraphics());
        
    }
    
    public ArrayList<Paintable> getPainting(){
        
        return this.painting;
        
    }

    public void setGameInfo(GameInfo gameInfo){

        this.gameInfo = gameInfo;

    }
    
    public void addPainting(Paintable p){
        
        this.painting.add(p);
        
        this.paintComponent(this.getGraphics());
        
    }
    
    public void paintComponent(Graphics g){
        
        super.paintComponent(g);
        
        this.setBackground(this.backgroundcolor);
        
        if(gameInfo != null){
        
            this.gameInfo.paint(g);
        
        }
        
        for(Paintable p : painting){
            
            p.paint(g);
            
        }
        
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        this.addPainting(new Punktlis(x,y,Color.MAGENTA));
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
