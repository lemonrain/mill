package mill;
import java.util.ArrayList;
import java.awt.Graphics;


public class Board extends Paintable{
    private int stoneAmount;
    private ArrayList<Node> nodes;
    private ArrayList<Bridge> bridges;
    
    // the board only has to "know" the nodes and the bridges because they are all we need to build a specific board
    // if we wanted to make the board bigger the coordinates of the nodes would change and there could be more of them
    // therefore the bridges would change and therefore the board would change but nothing more is needed
    public Board(){
        
        this.nodes = new ArrayList<Node>();
        
        this.bridges = new ArrayList<Bridge>();
        
    }
    // some getter and setter methods to get a hold of the private attributes
    public int getStoneAmount(){
        return this.stoneAmount;
    }

    public ArrayList<Node> getNodes(){
        return this.nodes;
    }
    public void setStoneAmount(int stoneAmount){
        this.stoneAmount = stoneAmount;
    }

    public void setNodeList(ArrayList<Node> nodeList){
        this.nodes = nodeList;
    }

    public ArrayList<Bridge> getBridges(){
        return this.bridges;
    }

    public void setBridgeList(ArrayList<Bridge> bridgeList){
        this.bridges = bridgeList;
    }
   
    //when we want to paint the board we need the bridges because they tell us how the lines have to be drawn and the nodes
    // because they tell us where we have to set the points where we can put a stone
    public void paint(Graphics g) {
        for(Bridge b: bridges){
            b.paint(g);
        }
        for(Node n: nodes){
            n.paint(g);
        }
    }
}