/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

//import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Mill-Project-Developing-Team
 */
public abstract class Paintable {
    
    public abstract void paint(Graphics g);
    
}
