/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Creates playable Game
 *
 * @author Mill-Project-Developing-Team
 */
public class Game implements Serializable {

    /**
     * Number of players in the game
     */
    private int playerAmount = 2;
    
    /**
     * current-Player-number
     */
    private int currentPlayer = 0;

    /**
     * gameboard of the current game
     */
    private Board board;

    /**
     * list of playercolors
     */
    private ArrayList<Color> colors;
    
    /**
     * list of players currently in play 
     */
    private ArrayList<Integer> players;

    /**
     * list of stones (from the Players)
     */
    private ArrayList<Stone> stones;

    /**
     * Graphic
     */
    private MillFrame millframe;

    /**
     * saves position of last click (if asked)
     */
    private int[] clickPos = new int[2];

    /**
     * boolean for waiting-Thread-loop
     */
    private volatile boolean stopped;

    /**
     * boolean for waiting-Thread-loop-2
     */
    private volatile boolean stopped2;
    
    /**
     * the node the last click was for
     */
    private Node currentlySelectetNode;

    /**
     * Thread which is just waiting until stopped is set to true
     */
    private Thread waitForMouseClickPosition = new Thread() {

        public void run() {

            stopped = false;

            while (!stopped) {
            }

        };

    };
    
    /**
     * Creates a new game with a specific board
     * 
     * @param board board for the game
     */
    public Game(Board board) {

        init(board);

    }

    /**
     * Creates a new game game with specific board and specific player amount
     *
     * @param board board for the game
     * @param playerAmount number of players for the game
     */
    public Game(Board board, int playerAmount) {

        // if the playerAmount isn't positive, it isn't changend from default (2)
        if (playerAmount > 0) {

            this.playerAmount = playerAmount;

        }

        init(board);

    }

    /**
     * List of Players of the game
     */
    private ArrayList<Player> playerList;
    
    /**
     * Is the game running?
     * true - game is running
     * false - game is over
     */
    private boolean running;
    
    /**
     * Creates a game with Clients via a Server
     * 
     * @param board gameboard
     * @param playerList list of the Sockets from the players
     */
    public Game(Board board, ArrayList<Player> playerList){
        
        this.playerList = playerList;
        
        this.playerAmount = this.playerList.size();
        
        this.running = true;
        
        initServerGame(board);
        
    }

    /**
     * Return the List of Sockets from the players
     * 
     * @return List of Sockets from the players
     */
    public ArrayList<Player> getPlayerList(){
        
        return this.playerList;
        
    }
    
    /**
     * Return the List of Numbers from the currently Players (that dont lose so far)
     * 
     * @return Numbers of current players
     */
    public ArrayList<Integer> getPlayers(){
        
        return this.players;
        
    }
    
    /**
     * initiate a new game to a specific board
     *
     * @param board board of the game
     */
    private void init(Board board) {

        this.millframe = new MillFrame();

        // saves last Mouse-Click-Coordinates
        millframe.getPanel().addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent evt) {

                clickPos[0] = evt.getX();
                clickPos[1] = evt.getY();

                stopped = true;

            }

        });

        // random first Player
        double rand = Math.random();
        this.currentPlayer = (int) (rand * this.playerAmount);

        GameInfo gi = new GameInfo(this.board, this.colors, this.currentPlayer, this.currentlySelectetNode, this.playerList.get(this.currentPlayer).getName());

        this.players = new ArrayList<Integer>();

        this.board = board;

        this.stones = new ArrayList<Stone>();

        int stoneAmount = board.getStoneAmount();
        
        this.colors = new ArrayList<Color>();

        // some player-colors
        colors.add(Color.WHITE);
        colors.add(Color.BLACK);
        colors.add(Color.RED);
        colors.add(Color.MAGENTA);
        colors.add(Color.CYAN);
        colors.add(Color.ORANGE);

        // split the amount of stones equaly to all players
        for (int i = 0; i < this.playerAmount; i++) {

            players.add(i);

            for (int j = 1; (j - 1) * this.playerAmount + i < stoneAmount; j++) {

                stones.add(new Stone(colors.get(i % colors.size()), i));

            }
        }
        
        this.millframe.getPanel().addPainting(gi);

        // Message for first player
        JOptionPane.showMessageDialog(null, "Player " + this.currentPlayer + " starts.", "First-Player-Message", JOptionPane.INFORMATION_MESSAGE);

        // start the game
        turn.start();
        
    }
    
    /**
     * initiate a new game to a specific board
     *
     * @param board board of the game
     */
    private void initServerGame(Board board) {

        // random first Player
        double rand = Math.random();
        this.currentPlayer = (int) (rand * this.playerAmount);

        this.players = new ArrayList<Integer>();

        this.board = board;

        this.stones = new ArrayList<Stone>();

        int stoneAmount = board.getStoneAmount();
        
        this.colors = new ArrayList<Color>();

        // some player-colors
        colors.add(Color.WHITE);
        colors.add(Color.BLACK);
        colors.add(Color.RED);
        colors.add(Color.MAGENTA);
        colors.add(Color.CYAN);
        colors.add(Color.ORANGE);

        // split the amount of stones equaly to all players
        for (int i = 0; i < this.playerAmount; i++) {

            players.add(i);

            for (int j = 1; (j - 1) * this.playerAmount + i < stoneAmount; j++) {

                stones.add(new Stone(colors.get(i % colors.size()), i));

            }
        }
        
        try {
            (new ObjectOutputStream(this.playerList.get(this.currentPlayer).getSocket().getOutputStream())).writeObject((new GameInfo(this.board, this.colors, this.currentPlayer, this.currentlySelectetNode, this.playerList.get(this.currentPlayer).getName())).addCurrentPlayer(true).addStartMessage());
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Message for first player
        
        // start the game
        serverTurn.start();
        
    }
    
    /**
     * Returns if the current game is still running
     * 
     * @return true - game is running
     */
    public boolean isRunning(){
        
        return this.running;
        
    }

    /**
     * Current-Player
     * 
     * @return current player number
     */
    public int getCurrentPlayer() {

        return this.currentPlayer;

    }

    /**
     * List of player-colors
     * 
     * @return list of player-colors
     */
    public ArrayList<Color> getColors() {

        return this.colors;

    }

    /**
     * currently selected Node
     * 
     * @return currently selected Node
     */
    public Node getCurrentlySelectetNode() {

        return this.currentlySelectetNode;

    }
    
    /**
     * Board of the game
     * 
     * @return gameboard
     */
    public Board getBoard(){
        
        return this.board;
        
    }

    /**
     * Thread for the turns
     */
    private Thread turn = new Thread() {

        public void run() {

            // if the current player is still in game, he get's his turn
            if (players.contains(currentPlayer)) {

                // if just one player is remainig, he wins the game
                if (players.size() == 1) {

                    win();

                } else {

                    // checks of a turn is possible
                    if (turnPossible()) {

                        boolean phase1 = false;

                        Stone waitingStone = null;

                        int remainingStones = 0;

                        // checks, if the player has an unused stone left (phase 1)
                        for (int i = 0; !phase1 && i < stones.size(); i++) {

                            Stone currentStone = stones.get(i);

                            if (currentStone.isWaiting() && currentStone.getPlayerNumber() == currentPlayer) {

                                phase1 = true;

                                waitingStone = currentStone;

                            }

                            if (currentStone.isInplay() && currentStone.getPlayerNumber() == currentPlayer) {

                                remainingStones++;

                            }

                        }

                        boolean validTurn = false;

                        boolean bridgeBuild = false;

                        if (phase1) {

                            // waits for a valid turn
                            while (!validTurn) {

                                mouseClickPosition();

                                Node near = nearestNode();

                                // if clicked on an empty node the stone comes in play to this position
                                if (near.isEmpty()) {

                                    waitingStone.placeInGame(near);

                                    bridgeBuild = isPartOfBridge(near);

                                    validTurn = true;

                                } else {

                                    // invalid turn (if we want to add a notifikation for that later)
                                    
                                }

                            }

                        } else {

                            // phase 2 or 3
                            
                            // waiting for valid turn
                            while (!validTurn) {

                                mouseClickPosition();

                                Node near1 = nearestNode();

                                if (near1.isEmpty() || near1.getStone().getPlayerNumber() != currentPlayer) {

                                    // invalid position
                                    // in the position has to be a stone of the current player
                                    // because this stone will be moved by the player
                                    
                                } else {

                                    currentlySelectetNode = near1;
                                    millframe.getPanel().updateUI();

                                    mouseClickPosition();

                                    Node near2 = nearestNode();

                                    if (!near2.isEmpty()) {

                                        // invalid turn
                                        // because node has to be empty, so that 
                                        // the stone can be moved here
                                        
                                    } else {

                                        boolean valid = true;

                                        // if there are more than 3 stones remaining,
                                        // we are still in phase 2
                                        if (remainingStones > 3) {
                                            
                                            valid = false;
                                            
                                        }//else phase 3
                                        
                                        // we only have to check, if selected 
                                        // node is valid in phase 2, because in 
                                        // phase 3 we can jump to every free node
                                        for (int i = 0; !valid && i < board.getBridges().size(); i++) {

                                            Bridge b = board.getBridges().get(i);

                                            if (b.getNodes().contains(near1) && b.getNodes().contains(near2)) {
                                                
                                                // turn is valid, when the node 
                                                // is adjecent to the "old" node, 
                                                // means that the difference of 
                                                // the indeces in a bridge is 1
                                                if (Math.abs(b.getNodes().indexOf(near1) - b.getNodes().indexOf(near2)) == 1) {

                                                    valid = true;

                                                }

                                            }

                                        }

                                        if (valid) {

                                            near1.getStone().move(near2);

                                            bridgeBuild = isPartOfBridge(near2);
                                            
                                            validTurn = true;

                                        } else {

                                            // invalid turn
                                            
                                        }

                                    }

                                }

                                currentlySelectetNode = null;

                            }

                        }

                        // when a bridge was build the player can remove a stone
                        if (bridgeBuild) {

                            millframe.getPanel().updateUI();

                            boolean justBridges = true;

                            // checks, if all stones of other players are in bridges
                            for (Stone s : stones) {

                                if (s.isInplay() && s.getPlayerNumber() != currentPlayer && s.getCurrentPosition() != null && !isPartOfBridge(s.getCurrentPosition())) {

                                    justBridges = false;

                                }

                            }

                            validTurn = !validTurn;

                            // waits for valid turn
                            while (!validTurn) {

                                mouseClickPosition();

                                Node near = nearestNode();

                                if (near.isEmpty() || near.getStone().getPlayerNumber() == currentPlayer || (isPartOfBridge(near) && !justBridges)) {

                                    // invalid turn
                                    // when node is empty, or stone of the 
                                    // player, or in a bridge (and there are not 
                                    // just bridges)
                                    
                                } else {

                                    near.getStone().delete();

                                    validTurn = true;

                                }

                            }

                        }

                    } else {
                        
                        // if there is no turn possible, the current player is 
                        // removed from the current players
                        
                        players.remove((Integer) currentPlayer);

                    }

                    // next players turn
                    currentPlayer = (currentPlayer + 1) % playerAmount;
                    
                    millframe.getPanel().updateUI();

                    run();

                }

            }

        }

    };

    /**
     * Removes a player if he has quit
     * 
     * @param s Socket of the player that quit
     */
    public void hasQuit(Player p){
        
        // removes player from current player list
        int index = this.playerList.indexOf(p);
        
        players.remove(players.indexOf(index));
        
        // if the player is the current player we stop waiting for a mouseclick
        if(index == this.currentPlayer){
            
            this.stopped = true;
            
        }
        
    }
    
    /**
     * test if a specific Socket is still alive
     * 
     * @param socket Socket which we want to test, if it is alive
     * @return true - Socket is still connectet / false - Socket isnt conectet
     */
    private boolean isAlive(Socket socket) {
        
        boolean alive = false;

        try {

            //try to send something to the Client
            (new ObjectOutputStream(socket.getOutputStream())).writeObject((new GameInfo(new StandardBoard(), null, 0, null, "")).setNotPaintable());

            alive = true;

        } catch (IOException ex) {
            
        }

        return alive;
        
    }
    
    /**
     * Thread for the turns of the game via a Server
     */
    private Thread serverTurn = new Thread() {

        public void run() {

            // if the current player is still in game, he get's his turn
            if (players.contains(currentPlayer) || players.size() == 1) {
                
                // if just one player is remainig, he wins the game
                if (players.size() == 1) {

                    // when someone won the game isnt running anymor
                    running = false;
                    
                    currentPlayer = players.get(0);
                    
                } else {
                    System.out.println(turnPossible());
                    // checks of a turn is possible
                    if (turnPossible()/* && Game.this.isAlive(playerList.get(currentPlayer))*/) {

                        boolean phase1 = false;

                        Stone waitingStone = null;

                        int remainingStones = 0;

                        // checks, if the player has an unused stone left (phase 1)
                        for (int i = 0; !phase1 && i < stones.size(); i++) {

                            Stone currentStone = stones.get(i);

                            if (currentStone.isWaiting() && currentStone.getPlayerNumber() == currentPlayer) {

                                phase1 = true;

                                waitingStone = currentStone;

                            }

                            if (currentStone.isInplay() && currentStone.getPlayerNumber() == currentPlayer) {

                                remainingStones++;

                            }

                        }

                        boolean validTurn = false;

                        boolean bridgeBuild = false;
                        
                        if (phase1) {

                            // waits for a valid turn
                            while (!validTurn && players.contains(currentPlayer)) {

                                waitForTurnCompleted.interrupt();
                                
                                mouseClickPosition();

                                Node near = nearestNode();

                                // if clicked on an empty node the stone comes in play to this position
                                if (near.isEmpty()) {

                                    waitingStone.placeInGame(near);

                                    bridgeBuild = isPartOfBridge(near);

                                    validTurn = true;

                                } else {

                                    // invalid turn (if we want to add a notifikation for that later)
                                    
                                }

                            }

                        } else {

                            // phase 2 or 3
                            
                            // waiting for valid turn
                            while (!validTurn && players.contains(currentPlayer)) {

                                waitForTurnCompleted.interrupt();
                                
                                mouseClickPosition();

                                Node near1 = nearestNode();

                                if (near1.isEmpty() || near1.getStone().getPlayerNumber() != currentPlayer) {

                                    // invalid position
                                    // in the position has to be a stone of the current player
                                    // because this stone will be moved by the player
                                    
                                } else {

                                    currentlySelectetNode = near1;
                                    
                                    waitForTurnCompleted.interrupt();
                                    
                                    mouseClickPosition();

                                    Node near2 = nearestNode();

                                    if (!near2.isEmpty()) {

                                        // invalid turn
                                        // because node has to be empty, so that 
                                        // the stone can be moved here
                                        
                                    } else {

                                        boolean valid = true;

                                        // if there are more than 3 stones remaining,
                                        // we are still in phase 2
                                        if (remainingStones > 3) {
                                            
                                            valid = false;
                                            
                                        }//else phase 3
                                        
                                        // we only have to check, if selected 
                                        // node is valid in phase 2, because in 
                                        // phase 3 we can jump to every free node
                                        for (int i = 0; !valid && i < board.getBridges().size(); i++) {

                                            Bridge b = board.getBridges().get(i);

                                            if (b.getNodes().contains(near1) && b.getNodes().contains(near2)) {
                                                
                                                // turn is valid, when the node 
                                                // is adjecent to the "old" node, 
                                                // means that the difference of 
                                                // the indeces in a bridge is 1
                                                if (Math.abs(b.getNodes().indexOf(near1) - b.getNodes().indexOf(near2)) == 1) {

                                                    valid = true;

                                                }

                                            }

                                        }

                                        if (valid) {

                                            near1.getStone().move(near2);

                                            bridgeBuild = isPartOfBridge(near2);
                                            
                                            validTurn = true;

                                        } else {

                                            // invalid turn
                                            
                                        }

                                    }

                                }

                                currentlySelectetNode = null;

                            }

                        }

                        // when a bridge was build the player can remove a stone
                        if (bridgeBuild) {

                            boolean justBridges = true;

                            // checks, if all stones of other players are in bridges
                            for (Stone s : stones) {

                                if (s.isInplay() && s.getPlayerNumber() != currentPlayer && s.getCurrentPosition() != null && !isPartOfBridge(s.getCurrentPosition())) {

                                    justBridges = false;

                                }

                            }

                            validTurn = !validTurn;

                            // waits for valid turn
                            while (!validTurn && players.contains(currentPlayer)) {

                                waitForTurnCompleted.interrupt();
                                
                                mouseClickPosition();

                                Node near = nearestNode();

                                if (near.isEmpty() || near.getStone().getPlayerNumber() == currentPlayer || (isPartOfBridge(near) && !justBridges)) {

                                    // invalid turn
                                    // when node is empty, or stone of the 
                                    // player, or in a bridge (and there are not 
                                    // just bridges)
                                    
                                } else {

                                    near.getStone().delete();

                                    validTurn = true;

                                }

                            }

                        }

                    } else {
                        
                        // if there is no turn possible, the current player is 
                        // removed from the current players
                        
                        players.remove((Integer) currentPlayer);

                    }

                    // next players turn
                    currentPlayer = (currentPlayer + 1) % playerAmount;
                    waitForTurnCompleted.interrupt();
                    
                    run();

                }

            }

        }

    };
    
    /**
     * Thread which is running during the completation of one turn so that no over Thread (like the Server) is running before the turn is completet (to prefent failes)
     */
    public Thread waitForTurnCompleted = new Thread() {

        public void run() {

            stopped2 = false;

            while(!Thread.currentThread().isInterrupted()){
                
            }
            
        };

    };

    /**
     * creates a new Thread which is waiting for turn completation
     */
    public void newWaitForTurnCompletedThread(){
        
        this.waitForTurnCompleted = new Thread() {

        public void run() {

            stopped2 = false;

            while(!Thread.currentThread().isInterrupted()){
                
            }

        };

    };
        
        this.waitForTurnCompleted.start();
        
    }
    
    /**
     * Checks if there is a possible turn for the current Player
     *
     * @return true - there is a possible turn / false - no possible turn left (player has lost)
     */
    private boolean turnPossible() {

        int remainingStones = 0;

        for (Stone s : stones) {

            // when the current player can bring a stone in game, he can still make a turn
            if (s.isWaiting() && s.getPlayerNumber() == this.currentPlayer) {

                if (this.emptySpaceRemaining()) {

                    return true;

                }

            }

            // when the stone isn't dead, then he is in play and is counted
            if (!s.isDead() && s.getPlayerNumber() == this.currentPlayer) {

                remainingStones++;

            }

        }

        // when the player has less than 3 stones remaining he has lost
        if (remainingStones < 3) {

            return false;

        }

        // when a player has 3 remaining stones he can jump, so there is a possible
        // turn, if there is a empty space
        if (remainingStones == 3) {

            if (this.emptySpaceRemaining()) {

                return true;

            } else {

                return false;

            }

        }

        // so if the player has no new stones for bringing in game and he can not 
        // jump: there are just legal moves, if he can move a stone along a bridge
        for (Bridge bridge : this.board.getBridges()) {
            
            for (int i = 0; i < bridge.getNodes().size(); i++) {

                Node n = bridge.getNodes().get(i);

                if (!n.isEmpty() && n.getStone().getPlayerNumber() == this.currentPlayer) {

                    if (i > 0 && bridge.getNodes().get(i - 1).isEmpty()) {

                        return true;

                    }

                    if (i < bridge.getNodes().size() - 1 && bridge.getNodes().get(i + 1).isEmpty()) {

                        return true;

                    }

                }

            }

        }

        // if there is no legal move
        return false;

    }

    /**
     * Is there at least one free spot on the board?
     *
     * @return true - there is a free spot / false - else
     */
    private boolean emptySpaceRemaining() {

        for (Node n : this.board.getNodes()) {

            if (n.isEmpty()) {

                return true;

            }

        }

        return false;

    }

    /**
     * Current Player won
     */
    private void win() {

        //winning message
        JOptionPane.showMessageDialog(null, "Player " + this.currentPlayer + " won.", "Winning-Player-Message", JOptionPane.INFORMATION_MESSAGE);

        int option = JOptionPane.showConfirmDialog(millframe, "Another round?", "Endgame-Question", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
        
        if(option == JOptionPane.YES_OPTION){
            
            millframe.dispose();
            
            Game game = new Game(new StandardBoard());
            
        }else{
            
            millframe.dispose();
            
        }
        
    }
    
    /**
     * waits for next mouse click
     */
    private void mouseClickPosition() {

        waitForMouseClickPosition.run();

        try {

            waitForMouseClickPosition.join();

        } catch (Throwable t) {

            System.out.println("Something went terribly wrong.");
            
        }

    }
    
    /**
     * Performing the next turn with the Infos from the Client
     * 
     * @param ci ClientInfo of the current player
     */
    public void next(ClientInfo ci){
        
        this.clickPos[0] = ci.getX();
        
        this.clickPos[1] = ci.getY();

        //stops the Thread that is waiting for the next clickposition
        this.stopped = true;
        
    }

    /**
     * Returns the closest Node to the last mouse-click-position
     * 
     * @return closest Node to last mouse-click-position
     */
    private Node nearestNode() {

        Node near = null;

        double minDistance = Double.MAX_VALUE;

        for (Node n : board.getNodes()) {

            double currentDistance = n.getDistance(clickPos[0], clickPos[1]);

            if (currentDistance < minDistance) {

                near = n;

                minDistance = currentDistance;

            }

        }

        return near;

    }

    /**
     * Checks, if with the Node n there is a complete bridge
     * 
     * @param n Node, for which it should check, if it complete a bridge
     * @return true - the Node n completes a bridge / false - it doesn't complete a bridge
     */
    private boolean isPartOfBridge(Node n) {

        if (n.isEmpty()) {

            return false;

        }

        for (Bridge b : board.getBridges()) {

            if (b.getNodes().contains(n)) {

                int playerOfStone = n.getStone().getPlayerNumber();

                boolean currentlyBridge = true;

                // is every Node in the bridge from the same player
                for (Node node : b.getNodes()) {

                    if (node.isEmpty() || node.getStone().getPlayerNumber() != playerOfStone) {

                        currentlyBridge = false;

                    }

                }

                if (currentlyBridge) {

                    return true;

                }

            }

        }

        return false;

    }

}
