/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Managed game informations and display them
 *
 * @author Mill-Project-Developing-Team
 */
public class GameInfo extends Paintable implements Serializable {

    /**
     * the board of the game
     */
    private Board board;

    /**
     * Playercolors
     */
    private ArrayList<Color> colors;

    /**
     * current player in the game
     */
    private int currentPlayer;
    
    /**
     * Name of the current player in the game
     */
    private String currentPlayerName = "";

    /**
     * selected node (phase 2, where you can move stones, so that is the
     * selected and marked stone that the player wants to move)
     */
    private Node currentlySelectedNode;

    /**
     * true - game is over / false - game is still running
     */
    private boolean end = false;

    /**
     * true - game just startet / false - game is already running
     */
    private boolean start = false;

    /**
     * should the client paint this gameInfo?
     */
    private boolean paintable = true;

    /**
     * is the Client the current player?
     */
    private boolean currentPlayerBol = false;

    /**
     * Nodes of the gameboard
     */
    private ArrayList<Node> nodes;

    /**
     * Bridges of the gameboard
     */
    private ArrayList<Bridge> bridges;

    /**
     * Create a GameInfo for transmitting all important informations from the
     * Server to a Client
     *
     * @param board gameboard of the current game
     * @param colors playercolors
     * @param currentPlayer current Player of the game
     * @param currentlySelectedNode selected Node (if in phase 2, where you can
     * move stones)
     */
    public GameInfo(Board board, ArrayList<Color> colors, int currentPlayer, Node currentlySelectedNode, String currentPlayerName) {

        this.board = board;

        this.nodes = board.getNodes();

        this.bridges = board.getBridges();

        this.colors = colors;

        this.currentPlayer = currentPlayer;

        this.currentlySelectedNode = currentlySelectedNode;
        
        this.currentPlayerName = currentPlayerName;

    }

    /**
     * Set this GameInfo to notPaintable, because there are no new informations
     * or you just check the connection to the Client with this GameInfo
     *
     * @return this GameInfo in notPaintable
     */
    public GameInfo setNotPaintable() {

        paintable = false;

        return this;

    }

    /**
     * Add the Information that the game is over and the Client will get an
     * endgame message
     *
     * @return this GameInfo with the information, that the game is over
     */
    public GameInfo addEndGameMessage() {

        end = true;

        return this;

    }

    /**
     * Add the information that the game just started and the Client will get an
     * start message
     *
     * @return this GameInfo with the information, that the game just began
     */
    public GameInfo addStartMessage() {

        start = true;

        return this;

    }

    /**
     * Is the game still running?
     *
     * @return true - game is still running / false - game ended
     */
    public boolean isEnd() {

        return end;

    }

    /**
     * Did the game just started?
     *
     * @return true - game just started / false - game is already running
     */
    public boolean isStart() {

        return start;

    }

    /**
     * Adds the information if the Client is the current Player
     *
     * @param currentPlayerBol is the Client the current Player?
     * @return This GameInfo with the Information, if the Client is the current
     * Player
     */
    public GameInfo addCurrentPlayer(boolean currentPlayerBol) {

        this.currentPlayerBol = currentPlayerBol;

        return this;

    }

    /**
     * Adds the information who is the current player
     *
     * @param player current player
     * @return This GameInfo with the information of the current Player
     */
    public GameInfo addCurrentPlayer(int player) {

        this.currentPlayerBol = (this.currentPlayer == player);

        return this;

    }

    /**
     * Is the Client the current Player?
     *
     * @return true - Client is current Player / false - Client isn't current
     * Player
     */
    public boolean isCurrentPlayer() {

        return this.currentPlayerBol;

    }

    /**
     * Get the index of the Current Player
     *
     * @return Index of current player
     */
    public int getCurrentPlayer() {

        return this.currentPlayer;

    }
    
    /**
     * Get the Name of the Current Player
     *
     * @return Index of current player
     */
    public String getCurrentPlayerName() {

        return this.currentPlayerName;

    }

    /**
     * Is painting the informations of the game on the Screen
     *
     * @param g Graphics
     */
    @Override
    public void paint(Graphics g) {

        // only paint the informations if the GameInfo is marked paintable
        if (paintable) {

            // paints the gameboard
            this.board.paint(g);
            
            // painting the Bridges
            for (Bridge b : bridges) {

                b.paint(g);

            }

            // painting the Nodes
            for (Node n : nodes) {

                n.paint(g);

            }

            // next player
            g.setColor(this.colors.get(this.currentPlayer));
            g.fillRect(0, 0, 20, 20);
            g.setColor(Color.BLACK);
            g.drawRect(0, 0, 20, 20);
            g.drawString("Player: " + this.currentPlayerName, 20, 15);

            // if the Client is the current player painting an info on the screen
            if (this.currentPlayerBol) {

                g.drawString("Your Turn :)", 20, 35);

            }

            //g.drawString("Quit :(", 700, 600);

            // currently selected node is displayed
            if (this.currentlySelectedNode != null) {

                Node n = this.currentlySelectedNode;

                g.setColor(Color.MAGENTA);
                g.fillOval(n.getXCoordinate() - 10, n.getYCoordinate() - 10, 20, 20);

                g.setColor(Color.green);
                g.drawOval(n.getXCoordinate() - 10, n.getYCoordinate() - 10, 20, 20);

            }

        }

    }

}
