package mill;

import java.awt.Graphics;
import java.awt.Color;
import java.io.Serializable;

//the stones have the attributes waiting, inplay, dead for there status in the game
//currentPosition as the node where it is currently placed
// playerNumber as the "owner" of the stone
public class Stone extends Paintable implements Serializable {
    private Color color; 
    private int playerNumber;
    private boolean inplay;
    private boolean waiting;
    private boolean dead;
    private Node currentPosition;

    //at first all the stones are waiting, because none of them are played
    // color and "owner" are given over to the constructor
    public Stone(Color color, int playerNumber)
    {
        this.color = color;
        this.inplay = false;
        this.waiting = true;
        this.dead = false;
        this.playerNumber = playerNumber;
    }

    //some getter and setter methods
    public Color getColor() {
        return this.color; 
    }
    
    public int getPlayerNumber(){
        return this.playerNumber;
    }

    public boolean isDead(){
        return this.dead;
    }

    public boolean isInplay(){
        return this.inplay;
    }

    public boolean isWaiting(){
        return this.waiting;
    }

    // to play the stone we have to change the status of the stone and have to give it to its first node
    // and give its first node to the stone
    public void placeInGame(Node n){
        this.inplay = true;
        this.waiting = false;
        n.setCurrentStone(this);
        placeStone(n);
    }
    
    // to move the stone we have to empty the current node and give the stone to a new one
    // to give the stone to the new node we just override the old one
    public void move(Node n){
        this.currentPosition.setCurrentStone(null);
        n.setCurrentStone(this);
        placeStone(n);
    }

    // to delete a stone we change the status and set the current node empty
    public void delete(){
        this.currentPosition.setCurrentStone(null);
        this.dead = true;
        this.inplay = false;
    }

    // give a node to the stone
    public void placeStone(Node n){
        this.currentPosition = n;
    }

    public Node getCurrentPosition(){
        return this.currentPosition;
    }

    // we paint a stone only when this function is called in nodes because than the node has a stone on it
    // we center it on the node and if the color is white we make a black circle around it so you can still see it
    public void paint(Graphics g) {
        g.setColor(this.color);
        g.fillOval(this.currentPosition.getXCoordinate()-15, this.currentPosition.getYCoordinate()-15, 30, 30);
        g.setColor(Color.BLACK);
        g.drawOval(this.currentPosition.getXCoordinate()-15, this.currentPosition.getYCoordinate()-15, 30, 30);
    }
}
