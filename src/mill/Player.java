package mill;
import java.io.Serializable;
import java.net.Socket;

public class Player {
    private String name;
    private Socket socket;

    public Player(String name, Socket socket){

        this.name = name;
        
        this.socket = socket;
        
    }

    public void setSocket(Socket socket){
        this.socket = socket;
    }

    
    public void setName(String name){
        this.name = name;
    }

    public Socket getSocket(){
        return this.socket;
    }


    public String getName(){
        return this.name;
    }
}