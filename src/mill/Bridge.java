package mill;
import java.awt.Color;
import java.util.ArrayList;
import java.awt.Graphics;
import java.io.Serializable;


public class Bridge extends Paintable implements Serializable {
    private ArrayList<Node> bridge;

    //array list with every possible mill (their coordinates) you could get -> run through it to see if there is a new mill
    public Bridge(ArrayList<Node> nodes){
        this.bridge = new ArrayList<Node>();
        for (Node n: nodes){
            this.bridge.add(n);
        }
    }

    public ArrayList<Node> getNodes(){
        return bridge;
    }

    // draw the mills to get the lines of the board
    public void paint(Graphics g) {
        
        g.setColor(Color.BLACK);
        
        for(int i=0; i<this.bridge.size()-1; i++){
            
            g.drawLine(this.bridge.get(i).getXCoordinate(), this.bridge.get(i).getYCoordinate(), this.bridge.get(i+1).getXCoordinate(), this.bridge.get(i+1).getYCoordinate());
            
        }
        
    }
}
