package mill;
import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

// nodes are the points where two or more lines meet on the gaming board
// with them we set how big the board is and how everything else works
public class Node extends Paintable implements Serializable {
    private Stone currentStone;
    private int xCoordinate;
    private int yCoordinate;

    // coordinates are given when created
    public Node(int x, int y){
        this.xCoordinate = x;
        this.yCoordinate = y;
        
    }

    // some getter and setter methods
    public int getXCoordinate(){
        return this.xCoordinate;
    }

    public int getYCoordinate(){
        return this.yCoordinate;
    }

    public void setCurrentStone(Stone currentStone){
        this.currentStone = currentStone;
    }

    public Stone getStone(){
        return this.currentStone;
    }

    // see if currently there is a stone on the board
    public boolean isEmpty(){
        if(this.currentStone == null){
            return true;
        }else{
            return false;
        }
    }

    // when a player does not get the exact point of the node or just clicks some where on the board 
    // we pick the point nearest to where tthe click was
    public double getDistance(int x, int y){
        double dist = 0;
        dist = Math.sqrt((x-this.xCoordinate)*(x-this.xCoordinate) + (y-this.yCoordinate)*(y-this.yCoordinate));
        return dist;
    }

    // paint the nodes centered on where the lines meet
    // when there is a stone on the node also paint the stone
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillOval(xCoordinate-5, yCoordinate-5, 10, 10);
        if(this.currentStone != null){
            this.currentStone.paint(g);
        }
    }
}