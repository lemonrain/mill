/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;

/**
 *
 * @author Mill-Project-Developing-Team
 */
public class MillFrame extends JFrame implements MouseListener {
    
    static Frame frame;
    
    private MillPanel panel;
    
    public MillFrame(){
        
        setTitle("Mühle");
        
        setSize(800,650);
        
        setVisible(true);
        
        this.panel = new MillPanel();
        
        add(this.panel);
        
        addMouseListener(this);
        
    }
    
    public MillPanel getPanel(){
        
        return this.panel;
        
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        /*int x = me.getX();
        int y = me.getY();
        int offx = panel.getX();
        System.out.println(offx);
        int offy = panel.getBounds().y;
        panel.addPainting(new Punktlis(x-offx,y-offy,Color.MAGENTA));
        System.out.println("jo");*/
    }

    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
