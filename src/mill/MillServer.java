/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;

/**
 *
 * @author Niklas
 */
public class MillServer {

    /**
     * Server
     */
    private ServerSocket serv;

    /**
     * List of the players that are waiting for a game
     */
    private ArrayList<Player> waitingPlayers = new ArrayList<Player>();

    /**
     * number of players required for a new mill-game
     */
    private int requiredPlayerAmount = 2;

    /**
     * board of the new started game
     */
    private Board board;

    /**
     * list of the currently running games
     */
    private ArrayList<Game> gameList = new ArrayList<Game>();

    private ArrayList<Player> activePlayers = new ArrayList<Player>();

    private Socket acceptedSocket = new Socket();
    
    private Player opponentPlayer;

    /**
     * start the server
     *
     * @param port Portnumber
     */
    public MillServer(int port) {

        try {

            serv = new ServerSocket(port);

            waitingForPlayers.start();

        } catch (IOException ex) {

            JOptionPane.showMessageDialog(null, "Something went terribly wrong, try to restart.", "Error-Message", JOptionPane.INFORMATION_MESSAGE);

        }

    }

    /**
     * Wait permanently for new players
     */
    private Thread waitingForPlayers = new Thread() {

        @Override
        public void run() {

            while (true) {

                //Socket socket = null;
                try {

                    // add new Clients to the waiting players
                    acceptedSocket = serv.accept();

                    Thread login = new Thread() {

                        public void run() {
                            
                            Socket socket = acceptedSocket;

                            LoginInfo li = new LoginInfo();

                            try {
                                li = (LoginInfo) (new ObjectInputStream(socket.getInputStream())).readObject();
                            } catch (ClassNotFoundException ex) {
                                li.addUsername("");
                                li.addPassword(0);
                            } catch (IOException ex) {
                                li.addUsername("");
                                li.addPassword(0);
                            }
                            System.out.println("tada");
                            LoginInfo logi = new LoginInfo();

                            if (li.registration()) {

                                if (!usernameAlreadyUsed(li.getUsername())) {

                                    try {
                                        Scanner sc = new Scanner(new FileInputStream("LoginData.txt"));
                                        
                                        String lines = "";
                                        
                                        while (sc.hasNextLine()) {
                                            lines = lines + sc.nextLine()  + "\n";
                                            //out.println(line);
                                        }
                                        
                                        PrintWriter out = new PrintWriter("LoginData.txt");

                                        out.println(lines + li.getUsername() + ":" + li.getPassword());

                                        sc.close();
                                        out.close();
                                        //Player player = newPlayer(username.getText(), socket);
                                        //this.activePlayers.add(player);
                                    } catch (FileNotFoundException e) {

                                    }

                                }

                            } /*else {

                                //logi.addRegistrationFailed();

                            }*/

                            if (alreadyRegistered(li.getUsername(), li.getPassword()) && !loggedIn(li.getUsername())) {
                                Player player = new Player(li.getUsername(), socket);
                                
                                activePlayers.add(player);

                                try {
                                    (new ObjectOutputStream(socket.getOutputStream())).writeObject((new LoginInfo()).addCurrentlyOnlinePlayers(createCurrentlyOnlinePlayerList()));
                                } catch (IOException ex) {
                                    removeActivePlayer(player);
                                }
                                
                                prepareNewGame(player);

                                
                                
                            } else {

                                if (loggedIn(li.getUsername())) {

                                    logi.addAlreadyOnline();

                                }

                                try {
                                    (new ObjectOutputStream(socket.getOutputStream())).writeObject(logi.addLoginFail());

                                    run();
                                } catch (IOException ex) {

                                }

                            }

                        }

                    };

                    login.start();

                    // das hier nach braucht man vermutlich dann nur in startNewGame
                    /*waitingPlayers.add(socket);
                    
                    // if there are enough waiting players: start a new game
                    if (waitingPlayers.size() >= requiredPlayerAmount) {

                        startNewGame();

                    }*/
                } catch (IOException ex) {
                    Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        }

    };

    private boolean alreadyRegistered(String name, int password) {
        boolean flag = false;
        //Reading the contents of the file
        try {
            Scanner sc = new Scanner(new FileInputStream("LoginData.txt"));
            while (sc.hasNextLine() && flag != true) {
                String line = sc.nextLine();
                String[] arrOfStr = line.split(":");
                if (arrOfStr[0].equals(name) && arrOfStr[1].equals(password + "")) {
                    flag = true;
                }
            }
            sc.close();
        } catch (FileNotFoundException e) {
        }
        return flag;
    }

    private boolean loggedIn(String name) {

        for (Player p : this.activePlayers) {

            if (p.getName().equals(name) && isAlive(p.getSocket())) {

                return true;

            }

        }

        return false;

    }

    private boolean usernameAlreadyUsed(String username) {
        boolean flag = false;
        //Reading the contents of the file
        try {
            Scanner sc = new Scanner(new FileInputStream("LoginData.txt"));
            while (sc.hasNextLine() && flag != true) {
                String line = sc.nextLine();
                String[] arrOfStr = line.split(":");
                if (arrOfStr[0].equals(username)) {
                    flag = true;
                }
            }
            sc.close();
        } catch (FileNotFoundException e) {
        }
        return flag;
    }

    private void prepareNewGame(Player p) {

        try {

            LoginInfo li = (LoginInfo) (new ObjectInputStream(p.getSocket().getInputStream())).readObject();

            if(!li.specialOpponentQuestion()){
            
            if (li.quit()) {

                (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentName("").addConfirmedNewGame());
                
                // remove Player from active Player list
                removeActivePlayer(p);

            } else {

                String opponent = li.getOpponentName();

                if (opponent != null) {

                    Player opponentPlayer = getPlayerByName(opponent);

                    if(opponent.equals(p.getName())){
                        
                        opponentPlayer = null;
                        
                    }
                    
                    boolean notFree = !isFree(opponentPlayer);
                    
                    if(notFree){
                        
                        //OpponentNotFound Information to Client
                        (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentNotFree().addCurrentlyOnlinePlayers(this.createCurrentlyOnlinePlayerList()));

                        prepareNewGame(p);
                        
                    }else{
                    
                    if (opponentPlayer != null) {
                        
                        try {
                            (new ObjectOutputStream(opponentPlayer.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentName(p.getName()));

                            // hide
                            try{
                                Thread.sleep(1000);
                            }catch(Throwable t){}
                            // end hide
                            
                            System.out.println("also auch hier?");
                            li = (LoginInfo) (new ObjectInputStream(opponentPlayer.getSocket().getInputStream())).readObject();
                            System.out.println("wirklich?");
                        } catch (IOException io) {
io.printStackTrace();
                            li = (new LoginInfo()).addQuit();

                        }

                        if (li.quit()) {

                            // remove Player from active Player list
                            removeActivePlayer(opponentPlayer);

                            (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentDenied());

                            prepareNewGame(p);
                            
                        } else {

                            if (li.confirmedNewGame()) {

                                try {

                                    (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentName(opponent).addConfirmedNewGame());

                                    ArrayList<Player> players = new ArrayList<Player>();

                                    players.add(p);
                                    players.add(opponentPlayer);

                                    startNewGame(players);

                                } catch (IOException io) {

                                    removeActivePlayer(p);

                                    try {

                                        (new ObjectOutputStream(opponentPlayer.getSocket().getOutputStream())).writeObject((new GameInfo(null, null, 0, null, "")).setNotPaintable().addCurrentPlayer(0).addEndGameMessage());

                                    } catch (IOException io2) {

                                        removeActivePlayer(opponentPlayer);

                                    }

                                }

                            } else {

                                //opponent Player denied
                                
                                this.opponentPlayer = opponentPlayer;
                                
                                Thread opponentLobby = new Thread(){
                                    
                                    public void run(){
                                        
                                        prepareNewGame(MillServer.this.opponentPlayer);
                                        
                                    }
                                    
                                };
                                
                                opponentLobby.start();
                                
                                //Opponent denied to play a game
                                (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentName(opponent).addOpponentDenied().addCurrentlyOnlinePlayers(this.createCurrentlyOnlinePlayerList()));

                                prepareNewGame(p);

                            }

                        }

                    } else {

                        //OpponentNotFound Information to Client
                        (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()).addOpponentNotFound().addCurrentlyOnlinePlayers(this.createCurrentlyOnlinePlayerList()));

                        prepareNewGame(p);

                    }

                    }
                    
                } else {

                    // Game with random opponent
                    waitingPlayers.add(p);
                    
                    // wichtig und sinnvoll, damit bei findGame in Client einlesen nicht corruptet
                    (new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject((new LoginInfo()));

                    // if there are enough waiting players: start a new game
                    if (waitingPlayers.size() >= requiredPlayerAmount) {

                        startNewGame();

                    }

                }

            }
            
            }
            //(new ObjectOutputStream(p.getSocket().getOutputStream())).writeObject(p);

        } catch (IOException io) {

            removeActivePlayer(p);

        } catch (ClassNotFoundException cnfe) {

            removeActivePlayer(p);

        }

    }
    
    private String createCurrentlyOnlinePlayerList(){
        
        String out = "";
        
        for(Player p: this.activePlayers){
            
            if(!isFree(p)){
             
                out += "🎲 ";
                
            }
            
            out += p.getName();
            
            out += "\n";
            
        }
        
        return out;
        
    }
    
    private boolean isFree(Player p){
        
        boolean notFree = this.waitingPlayers.contains(p);
                    
                    for(Game g : this.gameList){
                        
                        if(!notFree){
                            
                            notFree = g.getPlayerList().contains(p);
                            
                        }
                        
                    }
        
                    return !notFree;
                    
    }

    private void removeActivePlayer(Player p) {

        this.activePlayers.remove(p);

    }

    private Player getPlayerByName(String name) {

        for (Player p : this.activePlayers) {

            if (p.getName().equals(name)) {

                return p;

            }

        }

        return null;

    }

    /**
     * Check if a Client is still connected
     *
     * @param socket Client that connection we want to test
     * @return true - Client is still connected / false - Client isn't connected
     */
    private boolean isAlive(Socket socket) {

        boolean alive = false;

        try {

            //try to send something to the Client
            (new ObjectOutputStream(socket.getOutputStream())).writeObject((new GameInfo(new StandardBoard(), null, 0, null, "")).setNotPaintable());
//(new ObjectOutputStream(socket.getOutputStream())).writeInt(42);
            alive = true;

        } catch (IOException ex) {

        }

        return alive;

    }

    /**
     * Start a new game with the players currently waiting for a new game
     */
    private void startNewGame() {

        //filter the list of waiting players and check, if there are connectet
        this.waitingPlayers = this.waitingPlayers.stream().filter(player -> isAlive(player.getSocket())).collect(Collectors.toCollection(ArrayList::new));

        // if there are still enough waitingPlayers: start a new game
        if (this.waitingPlayers.size() >= this.requiredPlayerAmount) {

            // collect all players that will now start a new game
            ArrayList<Player> gamePlayers = new ArrayList<Player>();

            for (int i = 0; i < this.requiredPlayerAmount; i++) {

                gamePlayers.add(this.waitingPlayers.remove(0));

            }

            startNewGame(gamePlayers);

        }

    }

    private void startNewGame(ArrayList<Player> gamePlayers) {

        // neues Game mit Leuten starten
        if (this.board == null || true) {

            this.board = new StandardBoard();

        }

        // start a new game with the gameboard and the players
        Game game = new Game(board, gamePlayers);

        gameList.add(game);

        // start for every Client a new Thread which is waiting for informations
        // from the Client and sending them always the current informations
        for (Player p : gamePlayers) {

            //Socket s = p.getSocket();
            Thread playing = new Thread() {

                @Override
                public void run() {

                    try {
                        
                        while (game.isRunning()) {

                            ClientInfo ci = null;

                            try {

                                // waiting for Informations from the client
                                ci = (ClientInfo) (new ObjectInputStream(p.getSocket().getInputStream())).readObject();

                            } catch (IOException ex) {
                                Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ClassNotFoundException ex) {
                                Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            System.out.println(game.waitForTurnCompleted.getState() == State.NEW);

                            // if the Client quit: add the (quit)information to the game and wait until the current turn is over
                            if (ci.hasQuit()) {
                                System.out.println("has Quit");
                                activePlayers.remove(p);
                                System.out.println("removed");
                                game.newWaitForTurnCompletedThread();
                                game.hasQuit(p);
                                game.waitForTurnCompleted.join();
                                
                                
                                //this.interrupt();
                            } else {

                                // if the Client is the current Player: add his transmitted informations to the game and wait until the turn is completed
                                if (p.equals(game.getPlayerList().get(game.getCurrentPlayer()))) {

                                    game.newWaitForTurnCompletedThread();
                                    game.next(ci);
                                    game.waitForTurnCompleted.join();
                                }

                                // do nothing, if the Client isn't the current player
                                // because (if he doesn't quit) he can do nothing in the game
                            }
                            System.out.println(game.isRunning());

                            // send every Client the current game informations (just to the connected ones)
                            /*game.getPlayerList().stream().filter(socket -> MillServer.this.isAlive(socket)).forEach(socket -> {
                                    try {
                                        System.out.println("Server ginfo");
                                        System.out.println(game.getCurrentPlayer());
                                        (new ObjectOutputStream(socket.getOutputStream())).writeObject((new GameInfo(game.getBoard(), game.getColors(), game.getCurrentPlayer(), game.getCurrentlySelectetNode())).addCurrentPlayer(game.getPlayerList().indexOf(socket)));
                                        System.out.println(game.getCurrentPlayer());
                                    } catch (IOException ex) {
                                        Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                });*/
                            for (Player player : game.getPlayerList()) {

                                if (MillServer.this.isAlive(player.getSocket())) {

                                    try {
                                        System.out.println("Server ginfo");
                                        System.out.println(game.getCurrentPlayer());
                                        (new ObjectOutputStream(player.getSocket().getOutputStream())).writeObject((new GameInfo(game.getBoard(), game.getColors(), game.getCurrentPlayer(), game.getCurrentlySelectetNode(), game.getPlayerList().get(game.getCurrentPlayer()).getName())).addCurrentPlayer(game.getPlayerList().indexOf(player)));
                                        System.out.println(game.getCurrentPlayer());
                                    } catch (IOException ex) {
                                        Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                } else {

                                    // when connection is lost: the player is declared as he has quit
                                    if (game.getPlayers().contains(game.getPlayerList().indexOf(player))) {

                                        activePlayers.remove(player);
                                        System.out.println("removed");
                                        
                                        game.newWaitForTurnCompletedThread();
                                        game.hasQuit(player);
                                        game.waitForTurnCompleted.join();

                                        
                                    }

                                }
                            }

                        }

                        activePlayers.remove(p);
                                System.out.println("removed");
                        // if the game ended: send to every (still connected Client) the info that the game is over
                        game.getPlayerList().stream().filter(player -> MillServer.this.isAlive(player.getSocket())).forEach(player -> {
                            try {
                                (new ObjectOutputStream(player.getSocket().getOutputStream())).writeObject((new GameInfo(game.getBoard(), game.getColors(), game.getCurrentPlayer(), game.getCurrentlySelectetNode(), game.getPlayerList().get(game.getCurrentPlayer()).getName())).addEndGameMessage().addCurrentPlayer(game.getPlayerList().indexOf(player)));
                            } catch (IOException ex) {
                                Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        });
                        // close the connection
                        p.getSocket().close();

                    } catch (IOException ex) {
                        Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MillServer.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            };

            // start the Thread
            playing.start();

        }

    }

}
