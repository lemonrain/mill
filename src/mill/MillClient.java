package mill;

import java.awt.Dimension;
import java.awt.Window;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MillClient {

    private GameInfo ginfo = null;
    private MillFrame millFrame;
    private MillPanel millPanel;
    private int[] clickPos = new int[2];

    private Socket socket;

    /**
     * true - game ended / false - game is still running
     */
    private volatile boolean end = false;

    private InetSocketAddress address;

    /**
     * Starting the Client for a Mill-Game
     *
     * @param hostname Hostname of the Server
     * @param port Port-Number of the Server
     */
    public MillClient(String hostname, int port) {
        this.address = new InetSocketAddress(hostname, port);
        connect();
    }
    
    private MillClient(String hostname, int port, String username, int password) {
        this.address = new InetSocketAddress(hostname, port);
        this.username = username;
        this.password = password;
        connect();
    }

    private Thread serverInputThread;

    private LoginInfo currentLoginInfo = new LoginInfo();

    private String username = "";

    private int password;
    
    private String currentlyOnlinePlayers = "";

    private void connect() {
        try {
            // connecting to server
            this.socket = new Socket();
            this.socket.connect(this.address);
            // connected

            boolean loginsuccsess = true;
            
            if(username.equals("")){
                loginsuccsess = login();
            }else{
                
                LoginInfo li = (new LoginInfo()).addUsername(this.username).addPassword(this.password);
                
                (new ObjectOutputStream(socket.getOutputStream())).writeObject(li);
                System.out.println("vor a");
                li = (LoginInfo) (new ObjectInputStream(socket.getInputStream())).readObject();
                
                this.currentlyOnlinePlayers = li.getCurrentlyOnlinePlayers();
                
            }

            //Threaden?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // opens Dialoges for findig an Opponent
            

            //this.serverInputThread.start();

            if (loginsuccsess && findGame()) {

                // open the window for the Game
                millFrame = new MillFrame();
                millPanel = millFrame.getPanel();

                // remove old ginfo
                // get the input stream from the connected socket
                InputStream inputStream = socket.getInputStream();
                
                //System.out.println(inputStream.read());
                
                
                // create a DataInputStream so we can read data from it.
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                // read the information forwarded over the socket
                GameInfo ginfo = (GameInfo) objectInputStream.readObject();

                // add new ginfo
                millPanel.addPainting(ginfo);

                //painting
                millFrame.getPanel().updateUI();

                MillClient mc = this;

                // add an MouseListener to the Frame, for getting the click-positions during the game
                millFrame.getPanel().addMouseListener(new MouseAdapter() {

                    public void mouseClicked(MouseEvent evt) {

                        clickPos[0] = evt.getX();
                        clickPos[1] = evt.getY();

                        // by clicking in the bottomleft cornor you can quit the game
                        /*if (evt.getX() > 750 && evt.getY() > 600) {

                            int option = JOptionPane.showConfirmDialog(millFrame, "Do you really want to quit?", "Quit-Question", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

                            if (option == JOptionPane.YES_OPTION) {

                                end = true;

                                mc.reconnect();

                            }

                        }*/

                        // as long as the game doesn't end: send the informations of the click-Position to the Server
                        if (!end) {
                            try {
                                (new ObjectOutputStream(socket.getOutputStream())).writeObject(new ClientInfo(evt.getX(), evt.getY()));
                            } catch (IOException ex) {
                                Logger.getLogger(MillClient.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }

                    }
                });

                // by clicking on the cross the client quit the game
                this.millFrame.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent ev) {

                        end = true;
                        mc.reconnect();

                    }

                });

                //start the game-Thread
                this.runGame.start();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean login() {
        JTextField username = new JTextField();
        JTextField password = new JPasswordField();
        Object[] message = {
            "Username:", username,
            "Password:", password
        };
        Object[] options = {"OK", "Registration"};
        int option = JOptionPane.showOptionDialog(null, message, "Login", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        String usn = username.getText();
        String pwd = password.getText() + usn;
        int hash = pwd.hashCode();
        System.out.println("Mein Name ist: " + usn);
        LoginInfo li = (new LoginInfo()).addUsername(usn).addPassword(hash);

        boolean firstRound = true;

        while (li.registrationFailed() || firstRound) {
            System.out.println("ejjj");
            firstRound = false;
            
            boolean invalidUsername = false;
            
            if (option == JOptionPane.NO_OPTION) {

                //registration
                username = new JTextField();
                JTextField password1 = new JPasswordField();
                JTextField password2 = new JPasswordField();
                Object[] message2 = {
                    "Username:", username,
                    "Password:", password1,
                    "Password:", password2,};
                JOptionPane.showConfirmDialog(null, message2, "Login", JOptionPane.DEFAULT_OPTION);

                while (!password1.getText().equals(password2.getText())) {

                    JOptionPane.showConfirmDialog(null, "Passwords are not the same", "Message", JOptionPane.DEFAULT_OPTION);
                    JOptionPane.showConfirmDialog(null, message2, "Login", JOptionPane.DEFAULT_OPTION);

                }

                usn = username.getText();

                pwd = password1.getText();

                li.addUsername(usn).addPassword((pwd + usn).hashCode());

                li.addRegistration();

                if(usn.equals("") || usn.contains(":")){
                    
                    invalidUsername = true;
                    
                }
                
            }

            if (option == JOptionPane.CLOSED_OPTION) {

                li = li.addQuit();
                
            }

            try {

                if(!invalidUsername && !li.quit()){
                
                (new ObjectOutputStream(socket.getOutputStream())).writeObject(li);
                System.out.println("vor a");
                li = (LoginInfo) (new ObjectInputStream(socket.getInputStream())).readObject();
                System.out.println("hinter a");
                
                }
            } catch (IOException io) {
                System.out.println("Problem a ");
                io.printStackTrace();
            } catch (ClassNotFoundException cnfe) {
                System.out.println("Problem b");
            }
            
            if (li.registrationFailed()) {

                JOptionPane.showConfirmDialog(null, "Your Username is already used.", "Registration Fail", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);

            }
            
            if(invalidUsername){
                
                JOptionPane.showConfirmDialog(null, "Ivalid Username.", "Registration Fail", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                
            }
            
            if (li.loginFailed()) {

                if (li.alreadyOnline()) {

                    JOptionPane.showConfirmDialog(null, "You are already online.", "Message", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);

                } else {

                    JOptionPane.showConfirmDialog(null, "Your name or password is incorrect.", "Message", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);

                }

                //login();

                username = new JTextField();
                password = new JPasswordField();
                
                option = JOptionPane.showOptionDialog(null, message, "Login", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
                usn = username.getText();
                pwd = password.getText() + usn;
                hash = pwd.hashCode();

                li = (new LoginInfo()).addUsername(usn).addPassword(hash).addRegistrationFailed();

            }

            //option = JOptionPane.NO_OPTION;

            
            System.out.println("while");
        }

        this.currentlyOnlinePlayers = li.getCurrentlyOnlinePlayers();
        
        this.username = usn;

        this.password = hash;

        return !li.quit();
        
    }

    private boolean findGame() throws InterruptedException {
        System.out.println("c");
        
        this.serverInputThread = new Thread() {

                public void run() {

                    try {
                        currentLoginInfo = (LoginInfo) (new ObjectInputStream(socket.getInputStream())).readObject();
                        
                        if (currentLoginInfo.getOpponentName() != null && !currentLoginInfo.confirmedNewGame() && !currentLoginInfo.opponentDenied() && !currentLoginInfo.opponentNotFound() && !currentLoginInfo.opponentNotFree()) {

                            (new ObjectOutputStream(socket.getOutputStream())).writeObject((new LoginInfo()).addSpecialOpponentQuestion());
                            
                            currentLoginInfo = currentLoginInfo.addSpecialOpponentQuestion();
                        
                            int option = JOptionPane.showOptionDialog(null, currentLoginInfo.getOpponentName() + " wants to play with you :) \n Do you want to play with them?", "Play-Question", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"Absolutly", "No Way"}, "Absolutly");
                            System.out.println(option == JOptionPane.YES_OPTION);
                            if (option == JOptionPane.YES_OPTION) {

                                Window[] windows = Window.getWindows();
                                for (Window window : windows) {
                                    if (window instanceof JDialog) {
                                        JDialog dialog = (JDialog) window;
                                        if (dialog.getContentPane().getComponentCount() == 1
                                                && dialog.getContentPane().getComponent(0) instanceof JOptionPane) {
                                            dialog.dispose();
                                        }
                                    }
                                }
                                
                                (new ObjectOutputStream(socket.getOutputStream())).writeObject((new LoginInfo()).addConfirmedNewGame());
                                
                            } else {

                                (new ObjectOutputStream(socket.getOutputStream())).writeObject((new LoginInfo()).addOpponentDenied());

                                run();

                            }

                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                        Logger.getLogger(MillClient.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        ex.printStackTrace();
                        Logger.getLogger(MillClient.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            };
        
        this.serverInputThread.start();
        System.out.println("d");
        
        JTextArea field = new JTextArea(this.currentlyOnlinePlayers, 20, 20);
        
        field.setEditable(false);
        
        field.setAutoscrolls(true);
        
        JScrollPane pane = new JScrollPane(field);
        
        Object[] anzeige = {"Do you want to play with someone special?", "Currently online-Players:", pane};
        
        int answer = JOptionPane.showOptionDialog(null, anzeige, "special-question", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[]{"Special Opponent", "Random Opponent"}, "Random Opponent");

        if(answer == JOptionPane.CLOSED_OPTION && this.currentLoginInfo.specialOpponentQuestion()){
            
            answer = 42;
            
        }
        
        String opponent = null;

        LoginInfo li;

        boolean foundGame = true;
        System.out.println("found Game:" + foundGame);
        System.out.println(answer);
        try {

            //yes == special opponent
            switch (answer) {

                case JOptionPane.YES_OPTION:

                    JTextField text = new JTextField();

                    Object[] container = {"Please enter the Name of your Opponent.", text};
                    
                    int option = JOptionPane.showConfirmDialog(null, container, "Opponent-Name-Question", JOptionPane.DEFAULT_OPTION);

                    opponent = text.getText();
                    
                    //if (!opponent.equals(username)) {
                        
                        (new ObjectOutputStream(this.socket.getOutputStream())).writeObject((new LoginInfo()).addOpponentName(opponent));

                        //this.serverInputThread.wait();

                        while(this.serverInputThread.isAlive()){}
                        
                        li = this.currentLoginInfo;

                    /*} else {
                        
                        
                        
                        li = (new LoginInfo()).addOpponentNotFound();
                        
                    }*/

                    //li = (LoginInfo) (new ObjectInputStream(this.socket.getInputStream())).readObject();
                        
                        this.currentlyOnlinePlayers = li.getCurrentlyOnlinePlayers();
                        
                    if (li.opponentNotFound()) {
                        JOptionPane.showMessageDialog(null, "Opponent not found :(");
                        findGame();
                    }
                    if (li.opponentDenied()) {
                        JOptionPane.showMessageDialog(null, "Opponent does not want to play with you :(");
                        findGame();
                    }
                    if (li.opponentNotFree()) {
                        JOptionPane.showMessageDialog(null, "Opponent is currently playing :(");
                        findGame();
                    }
                    break;

                case JOptionPane.NO_OPTION:
                    (new ObjectOutputStream(this.socket.getOutputStream())).writeObject((new LoginInfo()).addOpponentName(opponent));
                    //this.serverInputThread.wait();
                    System.out.println("hier??");
                    while(this.serverInputThread.isAlive()){}
                    break;

                case JOptionPane.CLOSED_OPTION:
                    foundGame = false;
                    (new ObjectOutputStream(this.socket.getOutputStream())).writeObject((new LoginInfo()).addQuit());
                    //this.serverInputThread.interrupt();
                    System.out.println("sauber");
                    while(this.serverInputThread.isAlive()){}
                    break;

            }

        } catch (IOException io) {
            
            foundGame = false;

        }
        System.out.println(foundGame);
        while(this.serverInputThread.isAlive()/* && !this.serverInputThread.isInterrupted()*/){}
        System.out.println("weiter");
        //this.serverInputThread.interrupt();

/*        try{
            Object o = (new ObjectInputStream(socket.getInputStream())).readObject();
        }catch(Exception e){
            e.printStackTrace();
        }*/
        
        return foundGame;

    }

    /**
     * disconnect the Client if he quit and reconnect him to the server, if he
     * wants to reconnect
     */
    public void reconnect() {

        // send the information to the Server that the player quit
        try {
            (new ObjectOutputStream(socket.getOutputStream())).writeObject((new ClientInfo(0, 0)).iQuit());
        } catch (IOException ex) {
            Logger.getLogger(MillClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        // close connection to Server
        try {
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(MillClient.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        // Ask client if he wants to reconnect
        int option = JOptionPane.showConfirmDialog(millFrame, "Do you want to reconnect?", "Reconnect", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

        // close Frame
        millFrame.dispose();

        // if he wants to reconnect, reconnect :D
        if (option == JOptionPane.YES_OPTION) {

            MillClient mc = new MillClient(this.address.getHostName(), this.address.getPort(), this.username, this.password);

        }

    }

    /**
     * Thread for the running game
     */
    public Thread runGame = new Thread() {

        @Override
        public void run() {

            // wainting for getting a GameInfo from the server, so that the game can start
            while (ginfo == null) {

                try {
                    ginfo = (GameInfo) (new ObjectInputStream(socket.getInputStream()).readObject());
                } catch (IOException ex) {
                    Logger.getLogger(MillClient.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MillClient.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }

            int count = 0;

            // the game is running until the game ended (ginfo.isEnd()) or the
            // player quit (end)
            while (!ginfo.isEnd() && !end) {
                // adding the current GameInfo to the Panel
                millPanel.setGameInfo(ginfo);

                //painting
                millFrame.getPanel().updateUI();

                //if the game just started: make a start-message
                if (ginfo.isStart()) {

                    if (ginfo.isCurrentPlayer()) {

                        JOptionPane.showMessageDialog(millFrame, "You go first :D", "First-Player-Message", JOptionPane.INFORMATION_MESSAGE);

                    } else {

                        JOptionPane.showMessageDialog(millFrame, "Player " + ginfo.getCurrentPlayerName() + " starts.", "First-Player-Message", JOptionPane.INFORMATION_MESSAGE);

                    }

                }

                //getting a new gameInfo from the server
                try {
                    System.out.println("allfine");
                    ginfo = (GameInfo) (new ObjectInputStream(socket.getInputStream()).readObject());
                } catch (IOException ex) {
                    Logger.getLogger(MillClient.class
                            .getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MillClient.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }

            //when the game endet (not because the player quit)
            if (ginfo.isEnd()) {

                millPanel.setGameInfo(ginfo);

                //painting
                millFrame.getPanel().updateUI();

                // Information, if the player won
                if (ginfo.isCurrentPlayer()) {

                    JOptionPane.showMessageDialog(millFrame, "You won.", "Winning-Player-Message", JOptionPane.INFORMATION_MESSAGE);

                } else {

                    JOptionPane.showMessageDialog(millFrame, "Player " + ginfo.getCurrentPlayerName() + " won.", "Winning-Player-Message", JOptionPane.INFORMATION_MESSAGE);

                }

                // ask if he wants to play another round
                int option = JOptionPane.showConfirmDialog(millFrame, "Another round?", "Endgame-Question", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

                millFrame.dispose();

                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(MillClient.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

                if (option == JOptionPane.YES_OPTION) {

                    MillClient mc = new MillClient(address.getHostName(), address.getPort(), username, password);

                }

            }

        }

    };

}
