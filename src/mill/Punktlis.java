/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mill;

import java.awt.Color;
import java.awt.Graphics;

/**
 * just a little bit fun
 * 
 * @author Mill-Project-Developing-Team
 */
public class Punktlis extends Paintable {

    private int x;
    private int y;
    private Color c;
    
    public Punktlis(int x, int y, Color c){
        
        this.x = x;
        this.y = y;
        this.c = c;
        
    }
    
    @Override
    public void paint(Graphics g) {
        
        g.setColor(c);
        g.fillOval(x, y, 10, 10);
        
    }
    
    
    
}
