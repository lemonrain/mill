package mill;

import java.io.Serializable;

public class LoginInfo implements Serializable {
    private String username;
    private int password;
    private String opponentName = null;
    private boolean registration = false;
    private boolean loginFail = false;
    private boolean alreadyOnline = false;
    private boolean opponentNotFound = false;
    private boolean opponentDenied = false;
    private boolean opponentNotFree = false;
    private boolean quit = false;
    private boolean registrationFailed = false;
    private boolean confirmedNewGame = false;
    private boolean specialOpponentQuestion = false;
    private String currentlyOnlinePlayers = null;
    
    public LoginInfo addUsername(String username){
        
        this.username = username;
        
        return this;
        
    }
    
    public String getUsername(){
        
        return this.username;
        
    }
    
    public LoginInfo addPassword(int password){
        
        this.password = password;
        
        return this;
        
    }
    
    public int getPassword(){
        
        return this.password;
        
    }
    
    public LoginInfo addOpponentName(String opponent){
        
        this.opponentName = opponent;
        
        return this;
        
    }
    
    public String getOpponentName(){
        
        return this.opponentName;
        
    }
    
    public LoginInfo addRegistration(){
        
        this.registration = true;
        
        return this;
        
    }
    
    public boolean registration(){
        
        return this.registration;
        
    }
    
    public LoginInfo addLoginFail(){
        
        this.loginFail = true;
        
        return this;
        
    }
    
    public boolean loginFailed(){
        
        return this.loginFail;
        
    }
    
    public LoginInfo addAlreadyOnline(){
        
        this.alreadyOnline = true;
        
        return this;
        
    }
    
    public boolean alreadyOnline(){
        
        return this.alreadyOnline;
        
    }
    
    public LoginInfo addOpponentNotFound(){
        
        this.opponentNotFound = true;
        
        return this;
        
    }
    
    public boolean opponentNotFound(){
        
        return this.opponentNotFound;
        
    }
    
    public LoginInfo addOpponentDenied(){
        
        this.opponentDenied = true;
        
        return this;
        
    }
     
    public boolean opponentDenied(){
        
        return this.opponentDenied;
        
    }
    
    public LoginInfo addOpponentNotFree(){
        
        this.opponentNotFree = true;
        
        return this;
        
    }
    
    public boolean opponentNotFree(){
        
        return this.opponentNotFree;
        
    }
    
    public LoginInfo addQuit(){
        
        this.quit = true;
        
        return this;
        
    }
    
    public boolean quit(){
        
        return this.quit;
        
    }
    
    public LoginInfo addRegistrationFailed(){
        
        this.registrationFailed = true;
        
        return this;
        
    }
    
    public boolean registrationFailed(){
        
        return this.registrationFailed;
        
    }
    
    public LoginInfo addConfirmedNewGame(){
        
        this.confirmedNewGame = true;
        
        return this;
        
    }
    
    public boolean confirmedNewGame(){
        
        return this.confirmedNewGame;
        
    }
    
    public LoginInfo addSpecialOpponentQuestion(){
        
        this.specialOpponentQuestion = true;
        
        return this;
        
    }
    
    public boolean specialOpponentQuestion(){
        
        return this.specialOpponentQuestion;
        
    }
    
    public LoginInfo addCurrentlyOnlinePlayers(String currentlyOnlinePlayers) {
        
        this.currentlyOnlinePlayers = currentlyOnlinePlayers;
        
        return this;
        
    }
    
    public String getCurrentlyOnlinePlayers() {
        
        return currentlyOnlinePlayers;
        
    }

    
}