package mill;

import java.io.Serializable;


/**
 * Saves Information for transmitting them from Client to Server
 * 
 * @author Mill-Project-Developing-Team
 */
public class ClientInfo implements Serializable{

    /**
     * xCoordinate of last click from Client
     */
    private int xCoordinate;
    
    /**
     * yCoordinate of last click from Client
     */
    private int yCoordinate;
    
    /**
     * Did the Client quit the current game?
     */
    private boolean iQuit = false;

    /**
     * Information from Client
     * 
     * @param xCoordinate x-Coordinate from last click
     * @param yCoordinate y-Coordinate from last click
     */
    public ClientInfo(int xCoordinate, int yCoordinate){
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }
    
    /**
     * x-Coordinate from last click
     * 
     * @return x-Coordinate
     */
    public int getX(){
        
        return this.xCoordinate;
        
    }
    
    /**
     * y-Coordinate from last click
     * 
     * @return y-Coordinate
     */
    public int getY(){
        
        return this.yCoordinate;
        
    }
    
    /**
     * Adds the information that the Client quit to the ClientInfo
     * 
     * @return ClientInfo with additional info that the client quit
     */
    public ClientInfo iQuit(){
        
        this.iQuit = true;
        
        return this;
        
    }
 
    /**
     * Has the Client quit?
     * 
     * @return true - Client quit
     */
    public boolean hasQuit(){
        
        return this.iQuit;
        
    }
    
}
