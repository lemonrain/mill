package mill;
import java.io.Serializable;
import java.util.ArrayList;

//to maybe implement over boards we created a standard board like it is normally used in mill
// where you got 18 stones and 24 nodes
public class StandardBoard extends Board implements Serializable {
    public StandardBoard() {
        
        super();
        
        setStoneAmount(18);

        // get the coordinates (positions) of the nodes
        ArrayList<double[]> pos = getPosition();

        // make the nodes with their coordinates standing in pos
        // because we have 24 we need to it 24 times, therefore we also got 24 coordinates
        for (int i = 0; i< 24; i++){
            Node node = new Node((int)pos.get(i)[0], (int)pos.get(i)[1]);
            ArrayList<Node> nodeList = getNodes();
            nodeList.add(node);
            setNodeList(nodeList);
        }

        // make ArrayList of bridges 
        // in bridgeValues the indices of the nodes are saved which are in one mill
        ArrayList<Integer[]> bridgeValues = makeBridges();
        ArrayList<Node> nodeList = getNodes();
        // in total we have 16 bridges which we are saving together with their coordinates
        // for example bridge at (0,1,2) is saved with it's coordinates ((150,550),(400,550),(650,550))
        for (int j = 0; j < 16; j++){
            Integer[] val = bridgeValues.get(j);
            ArrayList<Node> nodes = new ArrayList<Node>();
            //adding coordinates to list
            nodes.add(nodeList.get(val[0]));
            nodes.add(nodeList.get(val[1]));
            nodes.add(nodeList.get(val[2]));
            Bridge bridge = new Bridge(nodes);
            ArrayList<Bridge> bridgeList = getBridges();
            bridgeList.add(bridge);
            setBridgeList(bridgeList);
        }
    }

    // our board is 500x500 times big starting at the left upper corner with the coordinates(150,50)
    // with this for loop we get the nine coordinates at the bottem
    public ArrayList<double[]> getPosition(){
        ArrayList<double[]> positions = new ArrayList<double[]>();
        
        int n = 3;
        
        for (int j = 0; j <= 2; j++){
            
            double[] posi = {150+ 500/6*j,550-500/6*j};
            positions.add(posi);
            for (int i = 0; i <= 1; i++) {
                double[] pos = {posi[0]+500/6*(n*(i+1)),posi[1]};
                
                positions.add(pos);
                
            }
            n--;
        }
        // with this for loop we get the six coordinates in the middle of the board leaving out the one in the middle of the board
        double[] pos10 = {150,300};
        positions.add(pos10);
        for (int i = 0; i <= 5; i++) {
            double[] pos = {pos10[0]+500/6*(i+1),pos10[1]};
            
            positions.add(pos); 
              
        }
        
        positions.remove(12);  

        n = 1;
        
        // with this for loop we get the upper nine coordinates
        for (int j = 2; j >= 0; j--){
            
            double[] posi = {150+500/6*j,50+500/6*j};
            positions.add(posi);
            for (int i = 0; i <= 1; i++) {
                double[] pos = {posi[0]+500/6*(n*(i+1)),posi[1]};
                
                positions.add(pos);
                
            }
            
            n++;
        }
        return positions;   
    }

    // we can count the nodes over their index in the nodeslist and here we decide which node is how part of a bridge
    public ArrayList<Integer[]> makeBridges(){
        ArrayList<Integer[]> bridges = new ArrayList<>();
        for (int i = 0; i <= 23; i = i + 3) {
            Integer[] bridge = {i,i+1,i+2};
            bridges.add(bridge);
        }
        Integer[] bridge9 = {0,9,21};
        bridges.add(bridge9);
        Integer[] bridge10 = {3,10,18};
        bridges.add(bridge10);
        Integer[] bridge11 = {6,11,15};
        bridges.add(bridge11);
        Integer[] bridge12 = {1,4,7};
        bridges.add(bridge12);
        Integer[] bridge13 = {16,19,22};
        bridges.add(bridge13);
        Integer[] bridge14 = {8,12,17};
        bridges.add(bridge14);
        Integer[] bridge15 = {5,13,20};
        bridges.add(bridge15);
        Integer[] bridge16 = {2,14,23};
        bridges.add(bridge16);
        return bridges;
    }   
}
